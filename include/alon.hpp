/*!
 * \file along.hpp
 * \brief Headers file with the class alon.
 *
 * Copyright(c) 2017 Ilya Shestakov
 */

#ifndef ALON_H
#define ALON_H

#include <string>
#include <vector>

/*!
 * \brief The alon class
 *
 * \details ALON is the additional long number.
 * The class to work with a long number.
 */
class alon {
public:
    alon();
    alon(unsigned size);
    alon(const alon &obj);
    ~alon();

public:
    std::vector<char> toBinary();

    /*!
     * \brief   abs
     * \return  absolutely number
     */
    alon abs();

    /*!
     * \brief   concat
     * \param   obj
     */
    void concat(const alon &obj);

    /*!
     * \brief even
     * \return true if this value is even
     */
    bool even();

    /*!
     * \brief   max_divisor
     * \param   b
     * \return
     */
    alon max_divisor(alon b);

    /*!
     * \brief   pow
     * \param   power
     * \return  a ^ power
     */
    alon pow(const unsigned power);

    /*!
     * \brief   pow_mod
     * \param   d
     * \param   m
     * \return  a ^ (d mod m)
     */
    alon pow_mod(const std::vector<char> &d, const alon &m);

    /*!
     * \brief   prime
     * \details Miller&Rabin primality test
     * \return  true if this value is primaly
     */
    bool prime();

    /*!
     * \brief   random
     * \param   lenght
     * \return
     */
    void random(unsigned lenght);

    /*!
     * \brief   size
     * \return  current lenght of number
     */
    unsigned size();

public: //unar operator
    alon operator =(const alon &right);
    alon operator =(const int   right);

    alon operator +(const alon &right);
    alon operator +(const int   right);

    alon operator *(const alon &right);
    alon operator *(const int   right);

    alon operator -(const alon &right);
    alon operator -(const int   right);

    alon operator /(const alon &right);
    alon operator /(const int   right);

    alon operator %(const alon &right);
    alon operator %(const int   right);

    alon operator +=(const alon &right);
    alon operator +=(const int   right);
    alon operator -=(const alon &right);
    alon operator -=(const int   right);

public: // comparison operator
    bool operator >(const alon &right);
    bool operator >(const int   right);
    bool operator <(const alon &right);
    bool operator <(const int   right);

    bool operator ==(const alon &right);
    bool operator ==(const int  right);
    bool operator !=(const alon &right);
    bool operator !=(const int  right);

    bool operator >=(const alon &right);
    bool operator >=(const int   right);
    bool operator <=(const alon &right);
    bool operator <=(const int   right);

public: // binary operator
    friend alon operator -(const alon &obj);
    friend alon operator +(const int left, const alon &right);
    friend alon operator -(const int left, const alon &right);
    friend alon operator *(const int left, const alon &right);
    friend alon operator /(const int left, const alon &right);
    friend alon operator %(const int left, const alon &right);

    friend bool operator <(const int left, const alon &right);
    friend bool operator >(const int left, const alon &right);

    friend bool operator ==(const int left, const alon &right);
    friend bool operator !=(const int left, const alon &right);
    friend bool operator <=(const int left, const alon &right);
    friend bool operator >=(const int left, const alon &right);

public:
    friend std::ostream &operator <<(std::ostream &out, const alon &obj);
    friend std::istream &operator >>(std::istream &in, alon &obj);

private:
    short    *m_number;
    unsigned  m_currentSize;
    unsigned  m_maxSize;
    bool      m_positive;

private:
    void reflect();
    void remove_space();
    bool isAbs(const alon &obj);
    alon div_mod(const alon &obj, alon &reminder);
    void init();
    void init(const short *number, const unsigned size);
};

#endif // ALON_H
