include(../app.pri )

QT       += testlib
QT       -= gui

TARGET    = tdd
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE  = app

SOURCES += test.cpp

LIBS += -lalon$${LIB_SUFFIX} \
        -lLibRSA$${LIB_SUFFIX}

LIBS += -L$${LIBS_PATH}/
