include(../../lib.pri)

QT -= gui

TARGET = LibRSA$${LIB_SUFFIX}
TEMPLATE = lib

DEFINES += RSA_LIB

SOURCES += rsa.cpp

HEADERS += $${INC_PATH}/rsa.hpp

LIBS += -lalon$${LIB_SUFFIX}
LIBS += -L$${LIBS_PATH}/
